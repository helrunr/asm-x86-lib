segment .data 

	stringFormat db "%c", 0x0	; notice using character format

segment .bss 

segment .text

    global main
	extern getchar
	extern printf

asm:
        enter   0,0 
        pusha

        call getChar		; function call
        call printString	; for testing
		
        popa
        xor eax, eax 
        leave                     
        ret

        ; function begins

getChar:
	enter 4,0               ; define local variable space
	pusha
	pushf

	call getchar
	mov [ebp-0x4], eax      ; getChar stores valie in eax
                            ; place eax in local variable

	popf
	popa
	mov eax, [ebp-0x4]       ; store the character in eax
	leave
	ret

printString:
	enter 0,0
	pusha
	pushf

	push eax
	push dword stringFormat
	call printf

	pop eax
	pop ecx

	popf
	popa
	leave
	ret
