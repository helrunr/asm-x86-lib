segment .data

    stringFormat db "%s", 0x0
    testString db "Test printString function", 0, 0xA

segment .bss

segment .text

    global main
    extern printf
    
main:
    enter 0,0
    pusha
    
    mov eax, testString
    call printString                ; call function
    
    popa
    xor eax, eax
    leave
    ret
    
    ; function begins
    
printString:
    enter 0,0
    pusha
    pushf
    
    push eax                       ; printf uses eax for string to print
    push dword stringFormat        ; printf requires a format string
    call printf
    
    pop eax
    pop ecx
    
    popf
    popa
    leave
    ret
    

    

