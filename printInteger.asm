segment .data

    intFormat db "%i", 0
    
segment .bss
segment .text

    global main
    extern printf
    
main:
    enter 0,0
    pusha
    
    call printInteger       ; function call
    
    popa
    xor eax,eax
    leave
    ret
    
    ; function begins

printInteger:
    enter 0,0
    pusha
    pushf
    
    push eax                ; printf prints what is in the eax register
    push dword intFormat    ; format string to print integer
    call printf
    
    pop ecx
    pop eax
    
    popf
    popa
    leave
    ret
