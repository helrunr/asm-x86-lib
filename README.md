# asm x86 lib

An ever-growing library of basic, but useful, assembly functions.

#### readInteger

* Reads a CLI input and stores it in eax.
   - Uses scanf

#### printInteger

* Prints an integer stored in eax.
    - Uses printf

#### printString

* Prints a string stored in eax.
    - Uses printf
    
#### readCharacter

* Read character from the CLI
    - Includes a modified formatString for printString to test functionality.
