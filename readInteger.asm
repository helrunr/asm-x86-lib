segment .data

    intFormat db "%i", 0

segment .bss
segment .text
    
    global main
    extern scanf
    
main:
    enter 0,0
    pusha
    
    call readInteger        ; function call
    
    popa
    xor eax, eax
    leave
    ret
    
    ; function begins
    
readInteger:
    enter 4,0               ; allocate space for local variable
    pusha                   ; push registers
    pushf                   ; push flags
    
    lea eax, [ebp-4]        ; pointer required for scanf
    push eax
    push dword int
    call scanf
    
    pop ecx
    pop eax
    
    popf
    mov eax, [ebp-4]        ; put value of local variable into eax for utilization
    leave ret
